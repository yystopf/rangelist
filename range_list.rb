class RangeList 

  attr_accessor :items, :custom_ranges

  def initialize 
    self.items = Array.new
    self.custom_ranges = Array.new
  end

  # insert a element and sorted items
  def insert_to_items(element)
    i = 1
    while (i < items.length + 1)
      if element < items[i-1]
        j = 0
        while (j < items.length - i + 1)
          tmp = items[items.length-1-j]
          items[items.length-j] = tmp
          j = j + 1
        end
        break
      elsif element == items[i-1]
        break
      end
      i = i + 1
    end 
    items[i-1] = element
  end

  # delete a element from items
  def delete_from_items(element)
    items.delete(element)
  end

  # build some custome ranges 
  def build_custom_ranges
    builded_custom_ranges = Array.new 
    left_item = 0
    right_item = 0
    items.each_with_index do |item, index|
      if index == 0
        left_item = item
        next 
      end
      if item - items[index-1] == 1 
        right_item = item 
		    next
	    else
        builded_custom_ranges << CustomRange.new(left_item, right_item)
        left_item = item 
      end
    end
    builded_custom_ranges << CustomRange.new(left_item, right_item)
    self.custom_ranges = builded_custom_ranges
  end

  # implement add 
  def add(range)
    return if range[0] == range[1]
    (range[0]..range[1]).to_a.each do |obj|
      self.insert_to_items(obj)
    end
    build_custom_ranges
  end 
  
  # implement remove
  def remove(range) 
    return if range[0] == range[1]
    (range[0]...range[1]).to_a.each do |obj|
      self.delete_from_items(obj)
    end
    build_custom_ranges
  end 
  
  # implement print
  def print 
    if items.length == 0
      puts 'It\'s a blank RangeList'
    else
      puts custom_ranges.collect{|r|r.to_string}.join(" ")
    end
  end

  class CustomRange 
    attr_accessor :left_item, :right_item 

    def initialize(left_item, right_item)
      self.left_item = left_item
      self.right_item = right_item
    end

    def to_string  
      "[#{left_item}, #{right_item})"
    end
  end
end